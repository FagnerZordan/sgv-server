# SGV - SISTEMA GERENCIADOR DE VÔO - README
===========================================

Ambiente de Desenvolvimento
-------------

> **Pre-requisitos:**

> - Java 8
> - Maven 3
> - Acesso ao repositório do sistema: https://bitbucket.org/FagnerZordan/sgv-server

> **Preparação do Banco de dados:**

> - Criar uma base de dados no Postgres chamada 'sgv', com username 'postgres' e senha 'postgres'
> - Executar o comando 'mvn liquibase:update -Plocal' para executar o liquibase

> **Variavel de ambiente:**
> - São necessárias 2 váriaveis de ambiente. A váriavel ENV que define qual perfil/ambiente está sendo utilizado e a variável JDBC_DATABASE_URL que é o caminho para acessar o seu banco de dados.
> - Setar os valores:
> -- ENV = local
> -- JDBC_DATABASE_URL = jdbc:postgresql://localhost:5432/sgv?user=postgres&password=postgres

> **Execução do sistema:**

> - Compilar o projeto: 'mvn clean install'
> - Rodar o projeto: 'mvn spring-boot:run'
