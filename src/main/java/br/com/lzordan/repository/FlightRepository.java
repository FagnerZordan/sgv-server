package br.com.lzordan.repository;

import br.com.lzordan.model.Flight;
import org.springframework.data.repository.CrudRepository;

/**
 * This interface is responsible for providing persistence methods related to the domain class
 * {@link Flight}
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
public interface FlightRepository extends CrudRepository<Flight, Long>{

  Flight[] findById(Long id);
}
