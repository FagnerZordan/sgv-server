package br.com.lzordan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This class represents the pilot of airplane.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Getter
@Entity
@AllArgsConstructor
@Table(name = "SGV_PILOT")
@SequenceGenerator(sequenceName = "SEQ_SGV_PILOT",
        name = "PILOT_SEQUENCE",
        initialValue = 1,
        allocationSize = 1)
public class Pilot implements Serializable {

  /** Field description */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PILOT_SEQUENCE")
  @Column(name = "ID_PILOT")
  private Long id;

  @Column(name = "NAME")
  private String name;

  @Column(name = "LAST_NAME")
  private String lastName;

  @Column(name = "AGE")
  private Long age;

  @Column(name = "FLIGHT_COUNT")
  private Long flightCount;

  public Pilot() {}

}
