package br.com.lzordan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This class represents flying airplane performing.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Getter
@Entity
@AllArgsConstructor
@Table(name = "SGV_AIRPLANE")
@SequenceGenerator(sequenceName = "SEQ_SGV_AIRPLANE",
        name = "AIRPLANE_SEQUENCE",
        initialValue = 1,
        allocationSize = 1)
public class Airplane implements Serializable {

  /** Field description */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AIRPLANE_SEQUENCE")
  @Column(name = "ID_AIRPLANE")
  private Long id;

  @Column(name = "NAME")
  private String name;

  @Column(name = "CAPACITY")
  private Long capacity;

  @Column(name = "WEIGHT")
  private Long weight;

  @Column(name = "SUPPLIER")
  private String supplier;

  private Airplane() {}

}
