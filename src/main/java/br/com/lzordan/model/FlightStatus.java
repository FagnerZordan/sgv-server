package br.com.lzordan.model;

import lombok.Getter;

/**
 * This Enumeration represents the flight status from airplane.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Getter
public enum FlightStatus {

  AIRBORNE("airborne"),
  LANDED("landed"),
  ACTIVE("active"),
  CANCELLED("cancelled"),
  FILED("filed"),
  DELAYED("delayed"),
  ON_TIME("on time");

  private final String flightStatus;

  FlightStatus(final String flightStatus) {
    this.flightStatus = flightStatus;
  }
}
