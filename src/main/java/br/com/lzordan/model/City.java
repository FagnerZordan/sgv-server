package br.com.lzordan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This class represents the city that can be origin or destiny.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Getter
@Entity
@AllArgsConstructor
@Table(name = "SGV_CITY")
@SequenceGenerator(sequenceName = "SEQ_SGV_CITY",
        name = "CITY_SEQUENCE",
        initialValue = 1,
        allocationSize = 1)
public class City implements Serializable {

  /** Field description */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CITY_SEQUENCE")
  @Column(name = "ID_CITY")
  private Long id;

  @Column(name = "STATE")
  private String state;

  @Column(name = "COUNTRY")
  private String country;

  public City() {}

}
