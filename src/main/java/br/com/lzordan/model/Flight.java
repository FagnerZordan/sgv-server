package br.com.lzordan.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;

/**
 * This class represents information about airplane flight.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Getter
@Entity
@AllArgsConstructor
@Table(name = "SGV_FLIGHT")
@SequenceGenerator(sequenceName = "SEQ_SGV_FLIGHT",
        name = "FLIGHT_SEQUENCE",
        initialValue = 1,
        allocationSize = 1)

public class Flight implements Serializable {

  /**
   * Field description
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FLIGHT_SEQUENCE")
  @Column(name = "ID_FLIGHT")
  private Long id;

  @Column(name = "DEPARTURE_TIME")
  private String departureTime;

  @Column(name = "ARRIVAL_TIME")
  private String arrivalTime;

  @ManyToOne
  @JoinColumn(name = "ORIGIN", referencedColumnName = "ID_CITY")
  private City origin;

  @ManyToOne
  @JoinColumn(name = "DESTINY", referencedColumnName = "ID_CITY")
  private City destiny;

  @ManyToOne
  @JoinColumn(name = "AIRPLANE", referencedColumnName = "ID_AIRPLANE")
  private Airplane airplane;

  @ManyToOne
  @JoinColumn(name = "PILOT", referencedColumnName = "ID_PILOT")
  private Pilot pilot;

  @Column(name = "FLIGHT_STATUS")
  private FlightStatus flightStatus;

  public Flight() {}

}
