package br.com.lzordan.service;

import br.com.lzordan.model.Flight;
import br.com.lzordan.repository.FlightRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for receiving requests regarding flight
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@Service()
public class FlightService {

  @Autowired
  private FlightRepository flightRepository;

  ObjectMapper mapper = new ObjectMapper();

  public List<String> getAllFlight() {
    List<String> listJsonFlight = new ArrayList<>();

    for(Flight flight : flightRepository.findAll()){
      try {
        listJsonFlight.add(mapper.writeValueAsString(flight));
      } catch (JsonProcessingException e) {
        e.printStackTrace();
      }
    }

    return listJsonFlight;
  }

  public String findById(Long id) {
    String jsonFlight = "";

    for(Flight flight : flightRepository.findById(id)){
      try {
        jsonFlight = mapper.writeValueAsString(flight);
      } catch (JsonProcessingException e) {
        e.printStackTrace();
      }
    }

    return jsonFlight;
  }
}

