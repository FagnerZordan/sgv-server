package br.com.lzordan.controller;

import br.com.lzordan.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class is responsible for receiving requests regarding flight
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */
@RestController
@RequestMapping(value = "/flight")
public class FlightController {

  @Autowired
  private FlightService flightService;

  @GetMapping("")
  public List<String> getAll() {
    return flightService.getAllFlight();
  }

  @GetMapping("/{id}")
  public String getById(@PathVariable("id") Long id) { return flightService.findById(id); }
}

