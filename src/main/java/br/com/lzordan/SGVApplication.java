package br.com.lzordan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The configuration class {@link SGVApplication} is responsible for starting the entire Spring Boot application and defining
 * general application configurations.
 *
 * @author Luiz Fagner Zordan
 * @version 1.0 14/04/2018
 */

@SpringBootApplication
public class SGVApplication {

	public static void main(String[] args){
		SpringApplication.run(SGVApplication.class, args);
	}
}
