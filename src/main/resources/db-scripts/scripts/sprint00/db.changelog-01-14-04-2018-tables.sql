--liquibase formatted sql

--changeset lzordan:00.01.01
--comment: create sequences
CREATE SEQUENCE SEQ_SGV_AIRPLANE MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_SGV_CITY MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_SGV_FLIGHT MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_SGV_PILOT MINVALUE 1 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1;
--rollback DROP SEQUENCE SGV.SEQ_SGV_AIRPLANE;
--rollback DROP SEQUENCE SGV.SEQ_SGV_CITY;
--rollback DROP SEQUENCE SGV.SEQ_SGV_FLIGHT;
--rollback DROP SEQUENCE SGV.SEQ_SGV_PILOT;

--changeset lzordan:00.01.02
--comment: create table SGV.SGV_AIRPLANE
CREATE TABLE SGV_AIRPLANE (
	ID_AIRPLANE NUMERIC(9,1) PRIMARY KEY,
	NAME VARCHAR(250) NOT NULL,
	CAPACITY NUMERIC(10,1) NOT NULL,
	WEIGHT NUMERIC(10,1) NOT NULL,
	SUPPLIER VARCHAR(150) NOT NULL
);
--rollback DROP TABLE SGV_AIRPLANE;

--changeset lzordan:00.01.03
--comment: create table SGV_CITY
CREATE TABLE SGV_CITY (
	ID_CITY NUMERIC(9,1) PRIMARY KEY,
	STATE VARCHAR(50) NOT NULL,
	COUNTRY VARCHAR(50) NOT NULL
);
--rollback DROP TABLE SGV_CITY;

--changeset lzordan:00.01.04
--comment: create table SGV_FLIGHT
CREATE TABLE SGV_FLIGHT (
	ID_FLIGHT NUMERIC(9,1) PRIMARY KEY,
	DEPARTURE_TIME VARCHAR(6) NOT NULL,
	ARRIVAL_TIME VARCHAR(6) NOT NULL,
	ORIGIN NUMERIC(9,1) NOT NULL,
	DESTINY NUMERIC(9,1) NOT NULL,
	AIRPLANE NUMERIC(9,1) NOT NULL,
	PILOT NUMERIC(9,1) NOT NULL,
	FLIGHT_STATUS NUMERIC(2,1) NOT NULL
);
--rollback DROP TABLE SGV_FLIGHT;

--changeset lzordan:00.01.05
--comment: create table SGV_PILOT
CREATE TABLE SGV_PILOT (
	ID_PILOT NUMERIC(9,1) PRIMARY KEY,
	NAME VARCHAR(250) NOT NULL,
	LAST_NAME VARCHAR(100) NOT NULL,
	AGE NUMERIC(3,1) NOT NULL,
	FLIGHT_COUNT NUMERIC(6,1) NOT NULL
);
--rollback DROP TABLE SGV_PILOT;